package com.x.wcrm.assemble.control.jaxrs.config;

import com.x.base.core.entity.JpaObject;
import com.x.base.core.entity.annotation.CheckPersist;
import com.x.base.core.project.annotation.FieldDescribe;
import com.x.wcrm.core.entity.WCrmConfig;
import com.x.wcrm.core.entity.tools.filter.QueryFilter;
import com.x.wcrm.core.entity.tools.filter.term.EqualsTerm;
import com.x.wcrm.core.entity.tools.filter.term.LikeTerm;
import org.apache.commons.lang3.StringUtils;
import org.apache.openjpa.persistence.jdbc.Index;

import javax.persistence.Column;

public class WrapInQueryConfig {

	@FieldDescribe("配置模块")
	private String configModule;

	@FieldDescribe("配置类别")
	private String configType;

	@FieldDescribe("配置名称")
	private String configName;

	@FieldDescribe("配置编码")
	private String configCode;

	@FieldDescribe("用于排列的属性，非必填，默认为createTime.")
	private String orderField = "createTime";

	@FieldDescribe("排序方式：DESC | ASC，非必填， 默认为DESC.")
	private String orderType = "DESC";
	
	private Long rank = 0L;
	
	public String getOrderField() {
		return orderField;
	}
	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}
	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public Long getRank() {
		return rank;
	}
	public void setRank(Long rank) {
		this.rank = rank;
	}

	public String getConfigModule() { return configModule; }

	public void setConfigModule(String configModule) { this.configModule = configModule; }

	public String getConfigType() { return configType; }

	public void setConfigType(String configType) { this.configType = configType; }

	public String getConfigName() { return configName; }

	public void setConfigName(String configName) { this.configName = configName; }

	public String getConfigCode() { return configCode; }

	public void setConfigCode(String configCode) { this.configCode = configCode; }

	/**
	 * 根据传入的查询参数，组织一个完整的QueryFilter对象
	 * @return
	 */
	public QueryFilter getQueryFilter() {
		QueryFilter queryFilter = new QueryFilter();		
		queryFilter.setJoinType( "and" );
		//组织查询条件对象
		/*if( StringUtils.isNotEmpty( this.getTitle() )) {
			queryFilter.addLikeTerm( new LikeTerm( "title", this.getTitle() ) );
		}
		if( StringUtils.isNotEmpty( this.getType())) {
			queryFilter.addEqualsTerm( new EqualsTerm( "type", this.getType() ) );
		}
		if( StringUtils.isNotEmpty( this.getOwner())) {
			queryFilter.addEqualsTerm( new EqualsTerm( "owner", this.getOwner() ) );
		}
		if( StringUtils.isNotEmpty( this.getDeleted() )) {
			if( "true".equalsIgnoreCase( this.getDeleted() )) {
				queryFilter.addEqualsTerm( new EqualsTerm( "deleted", true ) );
			}else {
				queryFilter.addEqualsTerm( new EqualsTerm( "deleted", false ) );
			}
		}*/

		if( StringUtils.isNotEmpty( this.getConfigModule())) {
			queryFilter.addLikeTerm( new LikeTerm( "configModule", this.getConfigModule() ) );
		}
		if( StringUtils.isNotEmpty( this.getConfigType())) {
			queryFilter.addLikeTerm( new LikeTerm( "configType", this.getConfigType() ) );
		}
		if( StringUtils.isNotEmpty( this.getConfigName())) {
			queryFilter.addLikeTerm( new LikeTerm( "configName", this.getConfigName() ) );
		}
		if( StringUtils.isNotEmpty( this.getConfigCode() )) {
			queryFilter.addLikeTerm( new LikeTerm( "configCode", this.getConfigCode() ) );
		}
		return queryFilter;
	}
}
