package com.x.wcrm.assemble.control.service;

import com.x.base.core.container.EntityManagerContainer;
import com.x.base.core.entity.JpaObject;
import com.x.base.core.entity.annotation.CheckPersistType;
import com.x.base.core.entity.annotation.CheckRemoveType;
import com.x.base.core.project.tools.ListTools;

import com.x.wcrm.assemble.control.Business;
import com.x.wcrm.core.entity.*;
import com.x.wcrm.core.entity.tools.filter.QueryFilter;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

class ConfigService {

	/**
	 * 根据项目的标识查询项目的信息
	 * @param emc
	 * @param flag  主要是ID
	 * @return
	 * @throws Exception 
	 */
	protected WCrmConfig get(EntityManagerContainer emc, String flag) throws Exception {
		Business business = new Business( emc );
		return business.configFactory().get( flag );
	}
	
	/**
	 * 根据条件查询符合条件的项目模板信息ID，根据上一条的sequnce查询指定数量的信息
	 * @param emc
	 * @param maxCount
	 * @param sequnce
	 * @param orderField
	 * @param orderType
	 * @param personName
	 * @param identityNames
	 * @param unitNames
	 * @param groupNames
	 * @return
	 * @throws Exception
	 */
	protected List<WCrmConfig> listWithFilter( EntityManagerContainer emc, Integer maxCount, String sequnce, String orderField, String orderType, String personName, List<String> identityNames, List<String> unitNames, List<String> groupNames, QueryFilter queryFilter ) throws Exception {
		Business business = new Business( emc );
		return business.configFactory().listWithFilter(maxCount, sequnce, orderField, orderType, personName, queryFilter);
	}



	/**
	 * 根据条件查询配置ID列表，最大查询2000条
	 * @param emc
	 * @param maxCount
	 * @param personName
	 * @param queryFilter
	 * @return
	 * @throws Exception
	 */
	public List<String> listAllConfigIds(EntityManagerContainer emc, int maxCount, String personName, QueryFilter queryFilter) throws Exception {
		Business business = new Business( emc );
		return business.configFactory().listAllConfigIds(maxCount, personName, queryFilter);
	}
}
