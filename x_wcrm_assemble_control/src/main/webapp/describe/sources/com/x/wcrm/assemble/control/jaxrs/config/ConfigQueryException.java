package com.x.wcrm.assemble.control.jaxrs.config;

import com.x.base.core.project.exception.PromptException;

class ConfigQueryException extends PromptException {

	private static final long serialVersionUID = 1859164370743532895L;

	ConfigQueryException(Throwable e ) {
		super("系统在查询系统配置信息时发生异常。" , e );
	}

	ConfigQueryException(Throwable e, String message ) {
		super("系统在查询系统配置信息时发生异常。Message:" + message, e );
	}
}
